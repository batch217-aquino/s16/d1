console.log("Hello World!")

let x = 3
let y = 10
let sum = x + y
console.log("Result of addition operators: " + sum)


let product = x * y
console.log("Result of multiplication operators: " + product)

let quotient = x / y
console.log("Result of division operators: " + quotient)

let remainder = y % x
console.log("Result of Modulo operators: " + remainder)

// SECTION

let assignNumber = 8

assignNumber = assignNumber + 2
console.log("The result of addition assignment operator: " + assignNumber)

assignNumber += 2
console.log("The result of addition assignment operator: " + assignNumber)

assignNumber -= 2
console.log("The result of substraction assignment operator: " + assignNumber)

assignNumber *= 2
console.log("The result of multiplication assignment operator: " + assignNumber)

assignNumber /= 2
console.log("The result of division assignment operator: " + assignNumber)

let mdas = 1 + 2 - 3 * 4 / 5
console.log("Result of mdas operation: " + mdas)

let pemdas = 1 + (2 - 3) * (4 / 5)
console.log("Result of pemdas operation: " + pemdas)


// incrementation and decrementation

// increment
let z = 1
let increment = ++z
console.log("Result of pre-increment: " + increment)
console.log("Result of post-increment: " + z)


increment = z++
console.log("Result of post-increment: " + increment)

// decrement
let decrement = --z
console.log("Result of pre-decrement: " + decrement)
console.log("Result of pre-decrement: " + z)


decrement = z--
console.log("Result of post-decrement: " + decrement)
console.log("Result of pre-decrement: " + z)


// Coercion

let numA = "10"
let numB = 12
let coercion = numA + numB
console.log(coercion)
console.log(typeof coercion)


let numC = 16
let numD = 14
let nonCoercion = numC + numD
console.log(nonCoercion)
console.log(typeof nonCoercion)

let numE = true + 1
console.log(numE)

let numF = false + 1
console.log(numF)

// Comparison Operators

let juan = "juan"
console.log(1 == 1)
console.log(1 == 2)
console.log(1 == '1')
console.log(0 == false)
console.log("juan" == "juan")
console.log("juan" == juan)



// inequality
console.log(1 != 1)
console.log(1 != 2)


// Strick Equality
console.log(1 === '1')
console.log("juan" === juan)
console.log(0 === false)

// Strick Inequality
console.log(1 !== '1')
console.log("juan" !== juan)
console.log(0 !== false)

// Relational operators

let a = 50
let b = 65
let greaterThan = a > b
console.log(greaterThan)

let lessThan = a < b
console.log(lessThan)

let gEqual = a >= b
console.log(gEqual)

let lEqual = a <= b
console.log(lEqual)


// Logical

let  legalAge = true
let registered = false
let allReqd = legalAge && registered
console.log("Result : " + allReqd)

let someReqd = legalAge || registered
console.log("Result: " + someReqd)

let someNotMet = !registered
console.log("Result: " + someNotMet)
